var amqp = require('amqp');
//var connection = amqp.createConnection({ host: "192.168.99.100", port: 5672 });
var connection = amqp.createConnection({ host: "192.168.0.112", port: 5672 });
var count = 1;
connection.on('ready', function () {
  var sendMessage = function(connection, queue_name, payload) {
    var encoded_payload = JSON.stringify(payload);
    connection.publish(queue_name, encoded_payload);
  }

  setInterval( function() {
    var test_message = 'TEST '+count
    sendMessage(connection, "hello", test_message)
    console.log("testing: " + new Date() + count);
    count += 1;
  }, 2000);

});
