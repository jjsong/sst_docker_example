var amqp = require('amqp');
var connection = amqp.createConnection({ host: "192.168.0.112", port: 5672 });
connection.on('ready', function () {
  connection.queue("hello", function(queue){
    queue.bind('#');
    queue.subscribe(function (message) {
      var encoded_payload = unescape(message.data)
      var payload = JSON.parse(encoded_payload)
      console.log(new Date()+  ' Recieved a message:')
      console.log(payload)
    })
  })
})
